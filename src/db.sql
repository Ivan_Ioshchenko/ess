CREATE DATABASE  IF NOT EXISTS `ess` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ess`;
-- MySQL dump 10.13  Distrib 5.6.10, for Win32 (x86)
--
-- Host: localhost    Database: ess
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `places`
--

DROP TABLE IF EXISTS `places`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `places` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `telephone` varchar(20) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `description` text,
  `address` text,
  `position` text,
  `place_type` varchar(45) DEFAULT NULL,
  `sport_id` int(11) NOT NULL,
  `visible` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `places`
--

LOCK TABLES `places` WRITE;
/*!40000 ALTER TABLE `places` DISABLE KEYS */;
INSERT INTO `places` VALUES (11,'3809494567',300,'Absolutely fantastic mountain offering great riding for ever style and level of rider. Great local services in and around the resort with lots going.','P51, Taranivka, Kharkiv Oblast, Ukraine','(49.6360347608653, 36.266052201390266)','place',1,'confirm'),(12,'3809494565',120,' Great freeriding with powder and full-on, freestyle terrain. Shame it\'s pricey and packed.','E40, Kharkiv Oblast, Ukraine','(49.28297628959971, 37.28247061371803)','place',1,'confirm'),(13,'093456121',100,'Before you ride the mountain, rip up the park or shred the powder, you\'ll want to check out the latest snowboarding gear by Burton, Lib Tech, Nike, Never.','ulitsa Pobedy, Bezliudivka, Kharkiv Oblast, Ukraine','(49.871509573865254, 36.310546696186066)','equipment',1,'confirm'),(14,'0984567123',0,'The Powder House Ski Shops are currently seeking dependable, hard-working staff members who possess a strong work ethic, a desire to work in a team environment and a great sense of humor.  If you are a reliable, skilled team player we would like to speak with you.  The Powder House Ski Shops employ approximately 60 seasonal employees in four shops, three of which are located within just a few feet of Alta Ski Area\'s lifts and our newest shop, the Wasatch Powder House, located just off of I-215 near the entry to Big Cottonwood Canyon.','ul. Naberezhnaya, Velyka Homil\'sha, Kharkiv Oblast, Ukraine, 63451','(49.554557029048695, 36.26092532649636)','equipment',1,'confirm'),(15,'8093533333',0,'Scuba diving equipment adapts you to the underwater world and makes you part of it. You do the diving, but your scuba gear makes it possible. That is, a mask doesnât see for you, but allows you to see underwater. A dive regulator doesnât breathe for you, but allows you to breathe underwater. A wetsuit doesnât make heat, but allows a body to more effectively retain its own heat.\r\nBrowse this section to learn more about how to choose scuba gear that :','Doroga, Kharkiv Oblast, Ukraine','(49.699437240848646, 36.66439821012318)','equipment',5,'confirm'),(16,'0935643876',50,'Paintball is a sport[2][3][4] in which players compete; in teams or individually, to eliminate opponents by tagging them with capsules containing water soluble dye and gelatin shell outside (referred to as paintballs) propelled from a device called a paintball marker (commonly referred to as a paintball gun). Paintballs are composed of a non-toxic, biodegradable, water soluble polymer. The game is regularly played at a sporting level with organized competition involving major tournaments, professional teams, and players.[5][6] Paintball technology is also used by military forces, law enforcement, para-military and security organizations to supplement military training, as well as playing a role in riot response, and non-lethal suppression of dangerous suspects.\r\n\r\nGames can be played on very hard floors in indoor fields, or outdoor fields of varying sizes. A game field is scattered with natural or artificial terrain, which players use for tactical cover. Game types in paintball vary, but can include capture the flag, elimination, ammunition limits, defending or attacking a particular point or area, or capturing objects of interest hidden in the playing area. Depending on the variant played, games can last from seconds to hours, or even days in scenario play.\r\n\r\nThe legality of paintball varies among countries and regions. In most areas where regulated play is offered, players are required to wear protective masks, and game rules are strictly enforced. Sometimes masks are not required.','Ð¢2104, Shestakove, Kharkiv Oblast, Ukraine','(49.995498813815885, 36.62072757259011)','place',3,'confirm');
/*!40000 ALTER TABLE `places` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sports`
--

DROP TABLE IF EXISTS `sports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sports`
--

LOCK TABLES `sports` WRITE;
/*!40000 ALTER TABLE `sports` DISABLE KEYS */;
INSERT INTO `sports` VALUES (1,'snowboarding'),(2,'ski jumping'),(3,'paintball'),(4,'skydiving'),(5,'diving'),(6,'banja jumping');
/*!40000 ALTER TABLE `sports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'user','erewr','sad@ukd.nr','qwerty'),(2,'sdfsf','sdfsdf','asdfsdf@ukr.net','qwerty'),(3,'Ivan','IOSHCHENKO','vanya88@ukr.net','qwerty');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-12-20 22:30:56
