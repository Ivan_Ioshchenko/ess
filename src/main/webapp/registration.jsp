<%@ page language="java" contentType="text/html; charset=UTF-8"%>

<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>

<html>

<c:set var="title">Registration
</c:set>
<%@ include file="/WEB-INF/jspf/head_reg.jspf"%>
<body>
	<%-- HEADER --%>
	<%@ include file="/WEB-INF/jspf/header_reg.jspf"%>
	<%-- HEADER --%>


	<div class="wrap">
		<!---start-content---->

		<div class="content">
			<div class="contact">

				<div class="span_2_of_3">
					<div class="reg-form">

						<div class="contact-form">
							<div class="reg-text">
								<h1>Registration</h1>

								<form name="form1" action="registration" method="post">


									<div>
										<span><label>Name</label></span>
										<div class="error">
											<c:out value="${errorMap.firsName}" />
										</div>

										<span><input name="firsName" type="text" class="text"
											maxlength="20" value="<c:out value="${form.firstName}" />"></span>
									</div>

									<div>
										<span><label>Surname</label></span>
										<div class="error">
											<c:out value="${errorMap.lastName}" />
										</div>
										<span><input name="lastName" type="text" class="text"
											maxlength="20" value="<c:out value="${form.lastName}" />"></span>
									</div>


									<div>
										<span><label>E-mail</label></span>
										<div class="error">
											<c:out value="${errorMap.email}" />
										</div>
										<span><input name="email" type="text" class="text"
											maxlength="20" value="<c:out value="${form.email}" />"></span>
									</div>
									<div>
										<span><label>Password</label></span>
										<div class="error">
											<c:out value="${errorMap.pswd}" />
										</div>
										<span><input type="password" name="pswd" class="text"
											id="pswd" value="<c:out value="${form.password}" />" /></span>
									</div>
									<div>
										<span><label>Confirm password</label></span>
										<div class="error">
											<c:out value="${errorMap.cpswd}" />
										</div>

										<span><input type="password" name="cpswd" class="text"
											id="cpswd" value="<c:out value="${form.confirmPassword}" />"></span>
									</div>
									<br/>
									<br/>



									<div>
										<span><input type="submit" value="Registration"></span>
									</div>


								</form>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="clear"></div>

		<div class="clear"></div>
	</div>
	<%@ include file="/WEB-INF/jspf/footer.jspf"%>
</body>
</html>