<%@ page language="java" contentType="text/html; charset=UTF-8"%>

<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>

<html>




<c:set var="title"> Point information
</c:set>
<head>
<%@ include file="/WEB-INF/jspf/headLoadVideo.jspf"%>
<script type="text/javascript"
	src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="scripts/downloadxml.js"></script>


</head>
<body style="margin: 0px; padding: 0px;" onload="initialize()">
	<%@ include file="/WEB-INF/jspf/header.jspf"%>


	<div class="wrap">
		<!---start-content---->
		<div class="content" >

			<div class="image group" style="width: 80%; margin: 0 auto;">
				<div class="grid span_2_of_3">
					<h3>About us</h3>
					<img style="height: 70%;" src="imagePlace?imagePlace=${imagePlace}"
						alt="Place" />
					<p></p>
					<p>${place.description}</p>

				</div>
				<div class="grid images_3_of_1">
					<h3>Contacts:</h3>
					<p>
					<h4>
						<span>Telephone: </span>
					</h4>
					${place.telephone}
					</p>

					<p>
					<h4>
						<span>Address: </span>
					</h4>
					${place.address}
					</p>

					<c:if test="${place.price !=0 }">
						<p>
						<h4>
							<span>Price: </span>
						</h4> ${place.price}
					</p>
					</c:if>
				</div>
			</div>
		</div>
		<!---End-content---->
		<div class="clear"></div>
	</div>

	<%@ include file="/WEB-INF/jspf/footer.jspf"%>
</body>
</html>
