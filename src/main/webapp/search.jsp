<%@ page language="java" contentType="text/html; charset=UTF-8"%>

<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>

<html>




<c:set var="title"> Create point
</c:set>
<head>
<%@ include file="/WEB-INF/jspf/headLoadVideo.jspf"%>
<script type="text/javascript"
	src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="scripts/downloadxml.js"></script>

<style type="text/css">
#map_canvas img,.google-maps img {
	max-width: none
}
</style>

<script type="text/javascript"
	src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBMOQIPcR0KBDZehK5vk0P7p_dcC1ZKQR4&sensor=true">
	
</script>
<script type="text/javascript">
	var map;
	function initialize() {

		var mapOptions = {
			center : new google.maps.LatLng(49.866316729538674, 36.2109375),
			zoom : 7,
			mapTypeId : google.maps.MapTypeId.ROADMAP
		};
		map = new google.maps.Map(document.getElementById("map_canvas"),
				mapOptions);
	}
</script>

<style type="text/css">
#map_canvas img,.google-maps img {
	max-width: none
}
</style>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.min.js"></script>

<script type="text/javascript" src="web/js/search.marker.js"></script>

</head>
<body style="margin: 0px; padding: 0px;" onload="initialize()">
	<%@ include file="/WEB-INF/jspf/header.jspf"%>

	<div class="container"></div>
	<div class="content">
		<div class="contact">
			<div class="section group">

				<div class="col span_1_of_3" style="width: 60%">
					<div class="contact_info">
						<h2>Organization on map</h2>
						<br />
						<div class="map">
							<div id="map_canvas" style="width: 100%; height: 70%"></div>
						</div>
					</div>

				</div>



				<div class="col span_2_of_3" style="width: 30%">
					<div class="contact-form">
						<h2>Search</h2>
						<div>
							<span><label>Text: </label> <input name="searchText"
								type="text" id="searchText" /></span>
						</div>

						<div>
							<span><label>Sport: </label> <select name="sportId"
								id="sportId">
									<c:forEach items="${listSport}" var="sport">
										<option value="${sport.id}">${sport.name}</option>
									</c:forEach>

							</select></span>
						</div>

						<div class="button">
							<span><a href="#" id="search">Search</a></span>
						</div>
					</div>

					<div class="listview_1_of_2 images_1_of_2">
						<div class="listimg listimg_2_of_1" id="imagePlace"
							style="width: 100%"></div>

						<div class="text list_2_of_1" style="width: 100%">
							<h3>Description</h3>
							<p id="describe"></p>
						</div>
					</div>

				</div>


			</div>
		</div>
	</div>





	<%@ include file="/WEB-INF/jspf/footer.jspf"%>
</body>
</html>
