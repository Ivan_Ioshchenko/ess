<%@ page language="java" contentType="text/html; charset=UTF-8"%>

<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>

<html>




<c:set var="title"> Create point
</c:set>
<head>
<%@ include file="/WEB-INF/jspf/headLoadVideo.jspf"%>
<script type="text/javascript"
	src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="scripts/downloadxml.js"></script>

<style type="text/css">
#map_canvas img,.google-maps img {
	max-width: none
}
</style>

<script type="text/javascript">
	//         

	// global "map" variable
	var map = null;
	var marker = null;

	var infowindow = new google.maps.InfoWindow({
		size : new google.maps.Size(150, 50)
	});

	// A function to create the marker and set up the event window function 
	function createMarker(latlng, name, html) {
		var contentString = html;
		var marker = new google.maps.Marker({
			position : latlng,
			map : map,
			zIndex : Math.round(latlng.lat() * -100000) << 5
		});

		google.maps.event.addListener(marker, 'click', function() {
			infowindow.open(map, marker);
		});
		google.maps.event.trigger(marker, 'click');

		$.ajax({
			type : "GET",
			url : "geoDecode",
			data : "location=" + marker.position,
			cache : false,
			success : function(data) {
				$('#address').text(data);
				$("#addressPlace").val(data);
			}
		});

		$("#position").val(marker.position);

		return marker;
	}

	function initialize() {
		// create the map
		var myOptions = {
			zoom : 7,
			center : new google.maps.LatLng(49.866316729538674, 36.2109375),
			mapTypeControl : true,
			mapTypeControlOptions : {
				style : google.maps.MapTypeControlStyle.DROPDOWN_MENU
			},
			navigationControl : true,
			mapTypeId : google.maps.MapTypeId.ROADMAP

		}
		map = new google.maps.Map(document.getElementById("map_canvas"),
				myOptions);

		google.maps.event.addListener(map, 'click', function() {
			infowindow.close();
		});

		google.maps.event.addListener(map, 'click', function(event) {
			//call function to create marker
			if (marker) {
				marker.setMap(null);
				marker = null;
			}
			marker = createMarker(event.latLng, "name", "<b>Location</b><br>"
					+ event.latLng);
		});

	}

	//
</script>



</head>
<body style="margin: 0px; padding: 0px;" onload="initialize()">
	<%@ include file="/WEB-INF/jspf/header.jspf"%>

	<div class="container"></div>
	<div class="content">
		<div class="contact">
			<div class="section group">

				<div class="col span_1_of_3">
					<div class="contact_info">
						<h2>Find Here</h2>
						<br />
						<div class="map">
							<div id="map_canvas" style="width: 450px; height: 350px"></div>
						</div>
					</div>

				</div>



				<div class="col span_2_of_3" style="width: 50%">
					<div class="contact-form">
						<h2>Contact Form</h2>

						<form action="addPlace" method="POST"
							enctype="multipart/form-data">

							<input type="hidden" id="position" value="" name="position" /> <input
								type="hidden" id="addressPlace" value="" name="addressPlace" />


							<div>
								<span><label>Telephone: </label> <input name="telephone"
									type="text" id="telephone" /></span>
							</div>
							<div>
								<span><label>Price: </label> <input name="price"
									type="text" id="price" /></span>
							</div>
							<div>
								<input type="radio" name="placeType" value="equipment"
									checked="checked">Equipment <input type="radio"
									name="placeType" value="place">Place
							</div>
							<div>
								<span><label>Description: </label> <textarea
										name="description" id="description"></textarea> </span>
							</div>

							<div>
								<span><label>Address: </label> <textarea disabled
										name="address" id="address"></textarea> </span>
							</div>

							<div>
								<span><label>Sport: </label> <select name="sportId">
										<c:forEach items="${listSport}" var="sport">
											<option value="${sport.id}">${sport.name}</option>
										</c:forEach>

								</select></span>
							</div>

							<div>
								<span><label>Image: </label></span> <span><input
									name="file" id="file" type="file"></span>
							</div>

							<div>
								<span><input type="submit" value="Add place"></span>
							</div>


						</form>

					</div>
				</div>
			</div>
		</div>
	</div>





	<%@ include file="/WEB-INF/jspf/footer.jspf"%>
</body>
</html>
