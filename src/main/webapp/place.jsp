<%@ page language="java" contentType="text/html; charset=UTF-8"%>

<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>

<html>

<c:set var="title"> Create point
</c:set>
<head>
<%@ include file="/WEB-INF/jspf/headLoadVideo.jspf"%>

<script type="text/javascript"
	src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBMOQIPcR0KBDZehK5vk0P7p_dcC1ZKQR4&sensor=true">
	
</script>
<script type="text/javascript">
	var map;
	function initialize() {

		var mapOptions = {
			center : new google.maps.LatLng(49.866316729538674, 36.2109375),
			zoom : 7,
			mapTypeId : google.maps.MapTypeId.ROADMAP
		};
		map = new google.maps.Map(document.getElementById("map_canvas"),
				mapOptions);
	}
</script>



<style type="text/css">
#map_canvas img,.google-maps img {
	max-width: none
}
</style>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.min.js"></script>

<script type="text/javascript" src="web/js/place.marker.js"></script>
</head>

<body style="margin: 0px; padding: 0px;" onload="initialize()">
	<%@ include file="/WEB-INF/jspf/header.jspf"%>
	<div id="map_canvas" style="width: 100%; height: 100%"></div>
	<%@ include file="/WEB-INF/jspf/footer.jspf"%>
</body>
</html>
