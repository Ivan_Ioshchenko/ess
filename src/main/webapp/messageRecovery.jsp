<%@ page language="java" contentType="text/html; charset=UTF-8"%>

<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>

<html>

<c:set var="title">Warning
</c:set>
<%@ include file="/WEB-INF/jspf/head_mess.jspf"%>
<body>
	<%-- HEADER --%>
	<%@ include file="/WEB-INF/jspf/header_reg.jspf"%>
	<%-- HEADER --%>


	<div class="wrap">
		<!---start-content---->

		<div class="content">
			<div class="contact">

				<div class="span_2_of_3">
					<div class="reg-form">

						<div class="contact-form">
							<div class="reg-text2">
								<h6>${messageSubject}</h6>

								<p>${messageText}</p>
								<div >
								<p >${recoveryText} </p>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="clear"></div>

		<div class="clear"></div>
	</div>
	<%@ include file="/WEB-INF/jspf/footer.jspf"%>
</body>
</html>