$(function() {
	var initFileUpload = function() {
		$('#file_upload')
				.fileUploadUI(
						{
							namespace : 'file_upload_1',
							fileInputFilter : '#file_1',
							dropZone : $('#drop_zone_1'),
							uploadTable : $('#files_1'),
							downloadTable : $('#files_1'),
							buildUploadRow : function(files, index) {
								return $('<tr><td>'
										+ files[index].name
										+ '<\/td>'
										+ '<td class="file_upload_progress"><div><\/div><\/td>'										
										+'<\/tr>');
							},
							buildDownloadRow : function(file) {
								return $('<tr id='
										+ file.name
										+ '><td>'
										+ file.name
										+ '<\/td><td><a href="javascript:deleteRow(\''+file.name+'\')"> <img src="web/images/remove.png" title="remove" /> <\/a> <\/td><\/tr>');
							}
						});
	};
	initFileUpload();
	
	
});

