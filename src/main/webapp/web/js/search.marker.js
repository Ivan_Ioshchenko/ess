var markersArray = [];

function viewDetail(id, description) {
	var img = $("<img />").attr('src', 'imagePlace?imagePlace='+id).load(
			function() {
				if (!this.complete || typeof this.naturalWidth == "undefined"
						|| this.naturalWidth == 0) {
					alert('broken image!');
				} else {
					$("#imagePlace").empty();
					$("#imagePlace").append(img);
				}
			});
	$('#describe').empty();
	$('#describe').append(description);
}

$(document).ready(function() {

	$('#search').click(function() {
		for ( var i = 0; i < markersArray.length; i++) {
			markersArray[i].setMap(null);
		}
		markersArray = [];
		textVal = $('#searchText').val();
		sportID = $('#sportId').val();

		$('#searchText').val("");

		url = "getPositions?search=" + textVal + "&sport=" + sportID;

		$.getJSON(url, function(json1) {
			var infoWindow = new google.maps.InfoWindow();

			$.each(json1, function(key, data) {
				var latLng = new google.maps.LatLng(data.lat, data.lng);
				// Creating a marker and putting it on the map
				var marker = new google.maps.Marker({
					position : latLng,
					title : data.title,
					map : map
				});
				if (data.placeType == "equipment") {
					marker.setIcon('web/images/ecvipment.png');
				} else {
					marker.setIcon('web/images/place.png');
				}
				markersArray.push(marker);

				// Creating a closure to retain the correct data, notice how I
				// pass the current data in the loop into the closure (marker,
				// data)
				(function(marker, data) {

					// Attaching a click event to the current marker
					google.maps.event.addListener(marker, "click", function(e) {
						// infoWindow.setContent(data.description);
						// infoWindow.open(map, marker);
						viewDetail(data.lat, data.description);

					});

				})(marker, data);

			});
		});
		// //

		return true;
	});

	$.getJSON('getPositions', function(json1) {
		var infoWindow = new google.maps.InfoWindow();

		$.each(json1, function(key, data) {
			var latLng = new google.maps.LatLng(data.lat, data.lng);

			// Creating a marker and putting it on the map
			var marker = new google.maps.Marker({
				position : latLng,
				title : data.title,
				map : map

			});

			if (data.placeType == "equipment") {
				marker.setIcon('web/images/ecvipment.png');
			} else {
				marker.setIcon('web/images/place.png');
			}
			markersArray.push(marker);

			// Creating a closure to retain the correct data, notice how I pass
			// the current data in the loop into the closure (marker, data)
			(function(marker, data) {

				// Attaching a click event to the current marker
				google.maps.event.addListener(marker, "click", function(e) {
					// infoWindow.setContent(data.description);
					// infoWindow.open(map, marker);
					viewDetail(data.lat,data.description);
				});

			})(marker, data);

		});
	});
});