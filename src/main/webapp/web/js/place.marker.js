var markersArray = [];

$(document).ready(function() {

	$('#search').click(function() {
		for ( var i = 0; i < markersArray.length; i++) {
			markersArray[i].setMap(null);
		}
		markersArray = [];
		textVal = $('#searchText').val();
		$('#searchText').val("");

		url = "getPositions?search=" + textVal;
		
		$.getJSON(url, function(json1) {
			var infoWindow = new google.maps.InfoWindow();

			$.each(json1, function(key, data) {
				var latLng = new google.maps.LatLng(data.lat, data.lng);
				// Creating a marker and putting it on the map
				var marker = new google.maps.Marker({
					position : latLng,
					title : data.title,
					map : map
				});
				if (data.placeType == "equipment") {
					marker.setIcon('web/images/ecvipment.png');
				} else {
					marker.setIcon('web/images/place.png');
				}
				markersArray.push(marker);

				// Creating a closure to retain the correct data, notice how I
				// pass the current data in the loop into the closure (marker,
				// data)
				(function(marker, data) {

					// Attaching a click event to the current marker
					google.maps.event.addListener(marker, "click", function(e) {
						infoWindow.setContent(data.description);
						infoWindow.open(map, marker);
					});

				})(marker, data);

			});
		});
		// //

		return true;
	});

	$.getJSON('getPositions', function(json1) {
		var infoWindow = new google.maps.InfoWindow();

		$.each(json1, function(key, data) {
			var latLng = new google.maps.LatLng(data.lat, data.lng);

			// Creating a marker and putting it on the map
			var marker = new google.maps.Marker({
				position : latLng,
				title : data.title,
				map : map

			});

			if (data.placeType == "equipment") {
				marker.setIcon('web/images/ecvipment.png');
			} else {
				marker.setIcon('web/images/place.png');
			}
			markersArray.push(marker);

			// Creating a closure to retain the correct data, notice how I pass
			// the current data in the loop into the closure (marker, data)
			(function(marker, data) {

				// Attaching a click event to the current marker
				google.maps.event.addListener(marker, "click", function(e) {
					infoWindow.setContent(data.description);
					infoWindow.open(map, marker);
				});

			})(marker, data);

		});
	});
});