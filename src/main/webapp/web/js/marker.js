$(document).ready(function() {

	$.getJSON('getPositions', function(json1) {
		var infoWindow = new google.maps.InfoWindow();

		$.each(json1, function(key, data) {
			var latLng = new google.maps.LatLng(data.lat, data.lng);

			var marker = new google.maps.Marker({
				position : latLng,
				title : data.title,
				map : map
			});

			(function(marker, data) {

				google.maps.event.addListener(marker, "click", function(e) {
					infoWindow.setContent(data.description);
					infoWindow.open(map, marker);
				});

			})(marker, data);

		});
	});
});
