
$(function() {
	form = $('form');

	$('form').submit(function() {
		form.find('.error').remove();

		var valid = 0;
	
		var fildUserName = form.find('input[name=firsName]');
		var fildUserSurname = form.find('input[name=lastName]');
		var fildUserEmail = form.find('input[name=email]');
		var fildPassword = form.find('input[name=pswd]');
		var fildCPassword = form.find('input[name=cpswd]');

		var check = $('input[name=check] ');
		if (validUserInf(fildUserName, 4)) {
			valid++;
		}

		if (validUserInf(fildUserSurname, 4)) {
			valid++;
		}

		if (validEmailFild(fildUserEmail)) {
			valid++;
		}
		if (validPassword(fildPassword, fildCPassword)) {
			valid++;
		}
		
	
		if (!check.is(':checked') ) {			
			
			{
				if($('input[name=check]:checked').length<=0)
				{
					check.before('<div class="error">You must select one</div>');
				
				}
			}
		}

		if (valid < 4) {
			return false;
		}

		return true;

	});

});

function validUserInf(fild, lenght) {

	fild.css({
		'border' : 'none'
	});

	if (fild.val() == '') {
		errorMessage(fild, 'Field must not be empty.');
		return false;
	} else {
		var pattern = new RegExp("^[A-z]+$");
		var value = fild.val();
		var res = pattern.test(value);

		if (res) {
			return true;
		} else {
			errorMessage(fild, 'Input correct values');
		}
	}

}


function validEmailFild(fild) {
	fild.css({
		'border' : 'none'
	});
	var pattern = new RegExp(
			/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	if (pattern.test(fild.val())) {
		return true;
	} else {
		errorMessage(fild, 'Input correct e-mail.');
		return false;
	}
}

function validPassword(pswd, cpswd) {
	cpswd.css({
		'border' : 'none'
	});
	pswd.css({
		'border' : 'none'
	});

	var pswdLenght = pswd.val().length;
	

	if (pswdLenght < 6) {
		errorMessage(pswd, 'Password must be at least 6 characters long.');
		return false;
	}

	if (pswd.val() == cpswd.val()) {
		return true;
	} else {
		errorMessage(cpswd, 'Passwords do not match.');
		return false;
	}
}

function errorMessage(fild, mes) {
	fild.before('<div class="error">' + mes + '</div>');
	fild.css({
		'border' : 'red 1px solid'
	});

}