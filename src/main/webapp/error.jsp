<%@ page language="java" contentType="text/html; charset=UTF-8"%>

<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>

<html>

<c:set var="title">Registration
</c:set>
<%@ include file="/WEB-INF/jspf/head_reg.jspf"%>
<body>
	<%-- HEADER --%>
	<%@ include file="/WEB-INF/jspf/header_reg.jspf"%>
	<%-- HEADER --%>


	<div class="wrap">
		<!---start-content---->
		<div class="content">
			<div class="error-page">
				<h3>404</h3>
				<h4>Page not found...!</h4>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<!---End-content---->
<%@ include file="/WEB-INF/jspf/footer.jspf"%>
</body>
</html>