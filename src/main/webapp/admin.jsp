<%@ page language="java" contentType="text/html; charset=UTF-8"%>

<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>

<html>
<c:set var="title">Admin panel</c:set>
<head>
<%@ include file="/WEB-INF/jspf/head_reg.jspf"%>

</head>
<body >
	<%@ include file="/WEB-INF/jspf/header.jspf"%>
	<div class="wrap">
		<div class="content">

			<div class="services">			

				<div class="section group">
					<c:forEach items="${listPlaces}" var="place">
						<div class="listview_2_of_2">
							<div class="list_2_of_2">
								<h4>Telephone: ${place.telephone} Sport:${place.sport.name}</h4>
								<h4>Description:</h4>
								<p>${place.description}</p>
								<h4>Address:</h4>
								<p>${place.address}</p>

								<div class="button">
									<span> <a href="${app}/placeAccesses?action=confirm&id=${place.id}">Confirm</a>
									</span> <span> <a href="${app}/placeAccesses?action=deny&id=${place.id}">Deny</a>
									</span>
								</div>
							</div>
						</div>
					</c:forEach>
				</div>
			</div>
		</div>
	</div>
	<%@ include file="/WEB-INF/jspf/footer.jspf"%>
</body>
</html>
