<%@ page language="java" contentType="text/html; charset=UTF-8"%>

<%@ include file="/WEB-INF/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf"%>

<html>

<c:set var="title">Log in
</c:set>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<%-- HEADER --%>
	<%@ include file="/WEB-INF/jspf/header_reg.jspf"%>
	<%-- HEADER --%>


	<div class="wrap">
		<!---start-content---->

		<div class="content">
			<div class="contact">

				<div class="span_2_of_3">
					<div class="reg-form">

						<div class="contact-form">
							<div class="reg-text">
								<h4>Log in</h4>

								<form action="login" method="post">
									<div>
										<span><label>E-mail</label></span>
										<div class="error">
											<c:out value="${errorMap.email}" />
										</div>
										<span><input name="email" type="text" class="text"
											maxlength="20" value="<c:out value="${email}" />"></span>
									</div>
									<div>
										<span><label>Password</label></span>
										<div class="error">
											<c:out value="${errorMap.pswd}" />
										</div>
										<span><input type="password" name="pswd" class="text"
											id="pswd" /></span>											
										<br>
										<br>
								<a class="form_ref" href="recovery.jsp">Recover password</a>
									</div>


									<div>
										<span><input type="submit" value="Log in"></span>
									</div>
								</form>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="clear"></div>

		<div class="clear"></div>
	</div>
	<%@ include file="/WEB-INF/jspf/footer.jspf"%>
</body>
</html>