package ua.kture.web;

public class Path {
	public static final String PAGE_INDEX = "index.jsp";
	public static final String PAGE_REGISTRATION = "registration.jsp";
	public static final String PAGE_LOGIN = "login.jsp";


	public static final String PAGE_MESSAGE = "message.jsp";
	public static final String PAGE_MESSAGE_RECOVERY = "messageRecovery.jsp";
	public static final String PAGE_RECOVERY = "recovery.jsp";
	
	
	public static final String PAGE_ADD_PLACE = "addPlace.jsp";
	public static final String PAGE_PLACES = "place.jsp";
	public static final String PAGE_PLACES_INFO = "infoPlace.jsp";

}
