package ua.kture.web.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.kture.entity.Place;
import ua.kture.img.PlaceImageStorage;
import ua.kture.service.PlaceService;
import ua.kture.service.SportService;
import ua.kture.web.Path;

@MultipartConfig
public class AddPlaceServlet extends HttpServlet  {
	
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		PlaceService placeService = (PlaceService) request.getServletContext()
				.getAttribute("placeService");
		SportService sportService = (SportService) request.getServletContext()
				.getAttribute("sportService");

		Place place = new Place();
		place.setTelephone(request.getParameter("telephone"));
		String price = request.getParameter("price").trim();
		if (price != null && price.length()!=0) {
			place.setPrice(Double.valueOf(price));
		}
		place.setAddress(request.getParameter("addressPlace"));
		place.setDescription(request.getParameter("description"));
		place.setPosition(request.getParameter("position"));
		
		String id = request.getParameter("sportId");
		
		place.setSport(sportService.get(Integer.valueOf(id)));
		place.setPlaceType(request.getParameter("placeType"));
		placeService.addPlace(place);
		
		
		new PlaceImageStorage().saveFile(request.getPart("file"), place.getPosition().substring(1,place.getPosition().indexOf(",")));

		response.sendRedirect(Path.PAGE_PLACES);
	
	}

}
