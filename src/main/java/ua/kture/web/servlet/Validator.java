package ua.kture.web.servlet;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import ua.kture.web.bean.FormRegistrationBean;

public class Validator {

	private Logger logger = Logger.getLogger(Validator.class);

	public Map<String, String> validateRegistrationForm(
			FormRegistrationBean formBean) {

		Map<String, String> map = new HashMap<String, String>();

		if (!validWithRegEx("firstName", formBean.getFirstName(), map,
				"^[A-z]+$")) {
			formBean.setFirstName("");
		}

		if (!validWithRegEx("lastName", formBean.getFirstName(), map,
				"^[A-z]+$")) {
			formBean.setLastName("");
		}

		if (!validWithRegEx("email", formBean.getEmail(), map,
				"^[-\\w.]+@([A-z0-9][-A-z0-9]+\\.)+[A-z]{2,4}$")) {
			formBean.setEmail("");
		}

		if (!validPassword("password", formBean.getPassword(),
				"confirmPassord", formBean.getConfirmPassword(), 6, map)) {
			formBean.setPassword("");
			formBean.setConfirmPassword("");
		}

		return map;
	}

	private boolean validWithRegEx(String fildName, String fildValue,
			Map<String, String> map, String regEx) {
		if (validNotEmpty(fildValue, fildName, map)) {
			if (validLenght(4, 30, fildValue, fildName, map)) {
				return validRegEx(fildValue, fildName, regEx, map);
			}
		}
		return false;

	}

	private boolean validPassword(String pswdName, String pswdValue,
			String cpswdName, String cpswdValue, int pswdLenght,
			Map<String, String> map) {
		if (validNotEmpty(pswdValue, pswdName, map)) {
			if (validLenght(6, 20, pswdValue, pswdName, map)) {
				if (!pswdValue.equals(cpswdValue)) {
					logger.debug("Passwords do not match -->> " + cpswdName);
					map.put(cpswdName, "Passwords do not match.");
				} else {
					return true;
				}
			}
		}
		return false;

	}

	private boolean validNotEmpty(String fildValue, String fildName,
			Map<String, String> map) {
		if (fildValue == null || fildValue.length() == 0) {
			logger.debug("Field must not be empty -->> " + fildName);
			map.put(fildName, "Field must not be empty.");
			return false;
		}
		return true;
	}

	private boolean validLenght(int minLenght, int maxLenght, String fildValue,
			String fildName, Map<String, String> map) {
		if (minLenght > fildValue.length()) {
			logger.debug("Min lenght -->>" + minLenght);
			map.put(fildName, "Min lenght = " + minLenght);
			return false;
		} else if (maxLenght < fildValue.length()) {
			logger.debug("Max lenght -->>" + minLenght);
			map.put(fildName, "Max lenght = " + maxLenght);
			return false;

		} else {
			return true;
		}
	}

	private boolean validRegEx(String fildValue, String fildName, String regEx,
			Map<String, String> map) {
		Pattern pattern = Pattern.compile(regEx);
		Matcher matcher = pattern.matcher(fildValue);
		if (!matcher.matches()) {
			logger.debug("Input correct values -->> " + fildName + ": "
					+ fildValue);
			map.put(fildName, "Input correct values.");

			return false;
		}
		return true;
	}

}
