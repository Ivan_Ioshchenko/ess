package ua.kture.web.servlet;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.kture.entity.Place;
import ua.kture.service.PlaceService;
import ua.kture.web.Path;

public class InfoPointServlet extends HttpServlet {
	Logger logger = Logger.getLogger(InfoPointServlet.class);
	private PlaceService placeService;

	@Override
	public void init(ServletConfig config) throws ServletException {
		ServletContext context = config.getServletContext();
		placeService = (PlaceService) context.getAttribute("placeService");
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.valueOf(request.getParameter("id"));

		Place place = placeService.getPlace(id);
		request.setAttribute("imagePlace", place.getPosition().substring(1,place.getPosition().indexOf(",")));
		request.setAttribute("place", place);
		request.getRequestDispatcher(Path.PAGE_PLACES_INFO).forward(request,
				response);
	}
}
