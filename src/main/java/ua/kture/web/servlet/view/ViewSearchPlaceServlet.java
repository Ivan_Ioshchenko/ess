package ua.kture.web.servlet.view;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.kture.entity.Sport;
import ua.kture.service.SportService;
import ua.kture.web.Path;

public class ViewSearchPlaceServlet extends HttpServlet {
	private SportService sportService;
	
	@Override
	 public void init(ServletConfig config) throws ServletException {
		ServletContext servletContext = config.getServletContext();
		sportService = (SportService) servletContext.getAttribute("sportService");
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		List<Sport> listSport = sportService.getListSport();
		request.setAttribute("listSport", listSport);
		request.getRequestDispatcher("search.jsp").forward(request,
				response);

	}

}
