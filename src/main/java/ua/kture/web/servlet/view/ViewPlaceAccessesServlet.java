package ua.kture.web.servlet.view;

import java.io.IOException;
import java.util.List;

import ua.kture.entity.Place;
import ua.kture.service.PlaceService;
import ua.kture.service.SportService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ViewPlaceAccessesServlet extends HttpServlet {
    private PlaceService placeService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        placeService = (PlaceService) servletContext.getAttribute("placeService");
    }
    
    @Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
    	List<Place> listPlaces = placeService.getListPlaceToConfirm();
    	request.setAttribute("listPlaces", listPlaces);
    	request.getRequestDispatcher("admin.jsp").forward(request, response);
    }
}
