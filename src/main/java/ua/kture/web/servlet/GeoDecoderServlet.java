package ua.kture.web.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.kture.service.GoogleMapService;

public class GeoDecoderServlet extends HttpServlet {
	private Logger logger = Logger.getLogger(GeoDecoderServlet.class);
	private GoogleMapService mapService = new GoogleMapService();

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String location = request.getParameter("location");
		logger.info("Geo location request -->> " + location);

		Map<String, String> params = new HashMap<>();
		params.put("language", "en");
		params.put("sensor", "false");
		params.put("latlng", formater(location));

		String address = mapService.getAddress(params);
		logger.info("Geo location address -->> " + address);

		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(address);

	}

	private String formater(String locale) {
		return locale.substring(1, locale.length() - 1);
	}

}
