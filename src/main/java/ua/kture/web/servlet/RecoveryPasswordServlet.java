package ua.kture.web.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.kture.entity.User;
import ua.kture.service.MailService;
import ua.kture.service.UserService;
import ua.kture.service.mail.MailMessages;
import ua.kture.service.mail.MailSubject;
import ua.kture.web.Path;
import ua.kture.web.SystemMessage;

public class RecoveryPasswordServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher(Path.PAGE_RECOVERY).forward(request,
				response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String email = request.getParameter("email");

		UserService userService = (UserService) request.getServletContext()
				.getAttribute("userService");
		if (userService.exist(email)) {
			User user = userService.get(email);

			String[] recipient = new String[] { email };

			String password = genPassword(8);
			new MailService().sendConfirmMessage(recipient,
					MailSubject.RECOVERY_PASSWORD,
					(MailMessages.RECOVERY_PASSWORD + password));

			userService.recoveryPassword(email, password);

			request.setAttribute("messageSubject",
					SystemMessage.SUBJECT_PASWORD_RECOVERY);
			request.setAttribute("messageText",
					SystemMessage.MESSAGE_PASWORD_RECOVERY);
			request.setAttribute("recoveryText",
					SystemMessage.MESSAGE_PASWORD_RECOVERY2);
			request.getRequestDispatcher(Path.PAGE_MESSAGE_RECOVERY).forward(
					request, response);

		} else {
			request.setAttribute("email", email);
			request.setAttribute("errorMessage", "This email not exist!");
			doGet(request, response);

		}
	}

	private String genPassword(int lenght) {
		StringBuffer buffer = new StringBuffer();
		String characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		for (int i = 0; i < lenght; i++) {
			double index = Math.random() * characters.length();
			buffer.append(characters.charAt((int) index));
		}
		return buffer.toString();
	}
}
