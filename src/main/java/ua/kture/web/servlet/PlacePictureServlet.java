package ua.kture.web.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.kture.img.ImageStorage;
import ua.kture.img.PlaceImageStorage;

public class PlacePictureServlet extends HttpServlet {
	private Logger logger = Logger.getLogger(PlacePictureServlet.class);

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) {
		response.setContentType("image/jpeg");

		try {
			OutputStream out = response.getOutputStream();
			ImageStorage storage = new PlaceImageStorage();

			String imagePlace = (String) request.getParameter("imagePlace");			

			File image = storage.getFileOrDefault(imagePlace + ".jpg");
			FileInputStream in = new FileInputStream(image);

			byte[] buf = new byte[1024];
			int len = 0;
			while ((len = in.read(buf)) >= 0) {
				out.write(buf, 0, len);
			}
			in.close();
			out.close();

			out.close();
		} catch (IOException e) {
			logger.error("Failed to load  image.", e);
			response.setStatus(500);
		}
	}
}
