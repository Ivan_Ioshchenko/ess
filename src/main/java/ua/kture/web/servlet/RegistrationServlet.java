package ua.kture.web.servlet;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.kture.entity.User;
import ua.kture.service.MailService;
import ua.kture.service.UserService;
import ua.kture.service.mail.MailMessages;
import ua.kture.service.mail.MailSubject;
import ua.kture.web.Path;
import ua.kture.web.SystemMessage;
import ua.kture.web.bean.FormRegistrationBean;

public class RegistrationServlet extends HttpServlet {

	private Logger logger = Logger.getLogger(RegistrationServlet.class);

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher(Path.PAGE_REGISTRATION).forward(request,
				response);

	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		logger.debug("Start registration.");
		FormRegistrationBean formBean = new FormRegistrationBean(
				request.getParameter("firsName"),
				request.getParameter("lastName"),
				request.getParameter("email"), request.getParameter("pswd"),
				request.getParameter("cpswd"));

		logger.debug("Form registration bean -->> " + formBean);

		Map<String, String> errorMap = new Validator()
				.validateRegistrationForm(formBean);

		logger.debug("Error map size -->> " + errorMap.size());
		if (errorMap.size() == 0) {
			UserService userService = (UserService) request.getServletContext()
					.getAttribute("userService");
			User user = convertFormBeanToUser(formBean);

			if (userService.exist(user.getEmail())) {
				errorMap.put("email", "User with this email exist");
				formBean.setConfirmPassword("");
				formBean.setPassword("");
				request.setAttribute("form", formBean);
				request.setAttribute("errorMap", errorMap);
				doGet(request, response);
			} else {
				String[] recipient = new String[] { user.getEmail() };
				new MailService().sendConfirmMessage(recipient,
						MailSubject.REGISTRATION,
						MailMessages.REGISTRATION_ON_SYSTEM);
				userService.add(user);
				HttpSession session = request.getSession();
				session.setAttribute("user", user);

				request.getRequestDispatcher(Path.PAGE_INDEX).forward(request,
						response);
			}
		}

		else {
			request.setAttribute("form", formBean);
			request.setAttribute("errorMap", errorMap);
			doGet(request, response);
		}

	}

	private User convertFormBeanToUser(FormRegistrationBean formBean) {

		return new User(formBean.getFirstName(), formBean.getLastName(),
				formBean.getEmail(), formBean.getPassword());
	}

}
