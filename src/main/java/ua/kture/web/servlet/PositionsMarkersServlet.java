package ua.kture.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ua.kture.entity.Place;
import ua.kture.service.PlaceService;

public class PositionsMarkersServlet extends HttpServlet {

	private Logger logger = Logger.getLogger(PositionsMarkersServlet.class);
	private PlaceService placeService;

	@Override
	public void init(ServletConfig config) throws ServletException {
		ServletContext context = config.getServletContext();
		placeService = (PlaceService) context.getAttribute("placeService");
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		JSONArray array = new JSONArray();
		String searchText = request.getParameter("search");

		String sport = request.getParameter("sport");

		List<Place> listPlace = null;
		if (searchText == null && sport == null) {
			listPlace = placeService.getListPlace();
		} else {
			if (searchText == null) {
				listPlace = placeService.getListPlace(sport);
			} else if (sport == null) {
				listPlace = placeService.getListPlace(searchText);
			} else {
				listPlace = placeService.getLictPlace(searchText, sport);
			}
		}

		for (Place place : listPlace) {
			JSONObject json;
			try {
				json = convertJSON(place);
				array.put(json);
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		out.print(array);
		out.flush();

	}

	private JSONObject convertJSON(Place place) throws JSONException {

		String latLng = place.getPosition().substring(1,
				place.getPosition().length() - 1);

		JSONObject json = new JSONObject();
		String lat = latLng.substring(0, latLng.indexOf(",")).trim();
		String lng = latLng.substring(latLng.indexOf(",") + 1, latLng.length())
				.trim();

		json.put("lat", lat);
		json.put("lng", lng);
		json.put("title", place.getTelephone());
		json.put("placeType", place.getPlaceType());

		String description = place.getDescription();
		if (description.length() > 60) {
			description = description.substring(0, 50) + "...";
		}
		json.put("description",
				description + "<div class=\"button\"><span> <a href=\"infoPoint?id=" + place.getId()
						+ "\"> More info</a></span></div>");
		return json;
	}

}
