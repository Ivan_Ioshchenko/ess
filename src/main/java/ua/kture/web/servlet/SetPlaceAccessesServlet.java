package ua.kture.web.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.kture.entity.Place;
import ua.kture.service.PlaceService;

public class SetPlaceAccessesServlet extends HttpServlet {
	private PlaceService placeService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        placeService = (PlaceService) servletContext.getAttribute("placeService");
    }
    
    @Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
    	String action = request.getParameter("action");
    	int id = Integer.valueOf(request.getParameter("id"));
    	Place place = placeService.getPlace(id);
    	
    	if(action.equals("confirm")){
    		place.setVisible("confirm");
    		placeService.update(place);
    	}
    	else{
    		placeService.remove(place);
    	}
    	
    	response.sendRedirect("admin");
    	
    }

}
