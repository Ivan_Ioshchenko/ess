package ua.kture.web;

public class SystemMessage {
	public static final String SUBJECT_WAIT = "Wait";
	public static final String SUBJECT_DENIED = "Denied";
	public static final String SUBJECT_PASWORD_RECOVERY = "Password recovery";

	public static final String MESSAGE_WAIT = "Please wait, expect confirmation of registration by the administrator!";
	public static final String MESSAGE_DENIED = "Sorry, Your request is denied";
	public static final String MESSAGE_PASWORD_RECOVERY = "Password has been sent to your email!";
	public static final String MESSAGE_PASWORD_RECOVERY2 = "After 10 seconds you will be redirected to the login page if it does not, click <a href="+"login.jsp"+" > this link </a>";
}
