package ua.kture.web.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import ua.kture.service.PlaceService;
import ua.kture.service.SportService;
import ua.kture.service.UserService;

public class Initializer implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {

	}

	@Override
	public void contextInitialized(ServletContextEvent contextEvent) {
		
		ServletContext context = contextEvent.getServletContext();

		context.setAttribute("userService", new UserService());
		context.setAttribute("sportService", new SportService());
		context.setAttribute("placeService", new PlaceService());

	}

	

}
