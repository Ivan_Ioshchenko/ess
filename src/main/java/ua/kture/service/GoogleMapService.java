package ua.kture.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import ua.kture.service.map.JsonReader;

public class GoogleMapService {
	private String baseUrl = "http://maps.googleapis.com/maps/api/geocode/json";
	private Logger logger = Logger.getLogger(GoogleMapService.class);

	public String getAddress(Map<String, String> params) {
		String formattedAddress = null;

		try {
			String url = baseUrl + "?" + encodeParamsToURL(params);		
			JSONObject response = JsonReader.read(url);
			JSONObject location = response.getJSONArray("results")
					.getJSONObject(0);
			formattedAddress = location.getString("formatted_address");

		} catch (IOException | JSONException e) {
			logger.error("Error when get address.", e);
		}

		return formattedAddress;

	}

	private String encodeParamsToURL(Map<String, String> paramsRequest)
			throws UnsupportedEncodingException {
		StringBuilder requestUrl = new StringBuilder();
		for (Map.Entry<String, String> entry : paramsRequest.entrySet()) {
			if (requestUrl.length() != 0) {
				requestUrl.append("&");
			}
			requestUrl.append(entry.getKey()).append("=");
			requestUrl.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
		}

		return requestUrl.toString();
	}
}
