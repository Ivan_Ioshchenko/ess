package ua.kture.service;

import java.util.List;
import ua.kture.db.JDBCTransactionManager;
import ua.kture.db.TransactionManager;
import ua.kture.db.TransactionOperation;
import ua.kture.db.DAO.SportDAO;
import ua.kture.db.DAO.MySQL.SportDAOImpl;

import ua.kture.entity.Sport;

public class SportService {
	private SportDAO sportDAO = new SportDAOImpl();
	private TransactionManager transactionManager = new JDBCTransactionManager();

	@SuppressWarnings("unchecked")
	public List<Sport> getListSport() {
		return (List<Sport>) transactionManager
				.doTransaction(new TransactionOperation() {

					@Override
					public Object execute() {
						return sportDAO.getListSport();
					}
				});
	}
	
	public Sport get(final int id){
		return (Sport) transactionManager.doTransaction(new TransactionOperation() {
			
			@Override
			public Object execute() {
				
				return sportDAO.get(id);
			}
		});
	}

}
