package ua.kture.service;

import java.util.List;

import ua.kture.db.JDBCTransactionManager;
import ua.kture.db.TransactionManager;
import ua.kture.db.TransactionOperation;
import ua.kture.db.DAO.PlaceDAO;
import ua.kture.db.DAO.MySQL.PlaceDAOImp;
import ua.kture.entity.Place;

public class PlaceService {
	private PlaceDAO placeDAO = new PlaceDAOImp();
	private TransactionManager transactionManager = new JDBCTransactionManager();

	public void addPlace(final Place place) {
		transactionManager.doTransaction(new TransactionOperation() {

			@Override
			public Object execute() {
				placeDAO.addPlace(place);
				return null;
			}
		});

	}

	public Place getPlace(final int idPlace) {
		return (Place) transactionManager
				.doTransaction(new TransactionOperation() {

					@Override
					public Object execute() {
						return placeDAO.getPlace(idPlace);
					}
				});

	}

	@SuppressWarnings("unchecked")
	public List<Place> getListPlace() {
		return (List<Place>) transactionManager
				.doTransaction(new TransactionOperation() {

					@Override
					public Object execute() {

						return placeDAO.getListPlace();
					}
				});

	}

	@SuppressWarnings("unchecked")
	public List<Place> getListPlaceToConfirm() {
		return (List<Place>) transactionManager
				.doTransaction(new TransactionOperation() {
					@Override
					public Object execute() {
						return placeDAO.getListPlaceToConfirm();
					}
				});

	}

	@SuppressWarnings("unchecked")
	public List<Place> getListPlace(final String searchText) {
		return (List<Place>) transactionManager
				.doTransaction(new TransactionOperation() {

					@Override
					public Object execute() {
						return placeDAO.getListPlace(searchText);
					}
				});

	}

	@SuppressWarnings("unchecked")
	public List<Place> getLictPlace(final String searchText, final String sport) {
		return (List<Place>) transactionManager
				.doTransaction(new TransactionOperation() {

					@Override
					public Object execute() {
						return placeDAO.getListPlace(searchText, sport);
					}
				});

	}

	public void update(final Place place) {
		transactionManager.doTransaction(new TransactionOperation() {

			@Override
			public Object execute() {
				placeDAO.update(place);
				return null;
			}
		});
	}

	public void remove(final Place place) {
		transactionManager.doTransaction(new TransactionOperation() {

			@Override
			public Object execute() {

				placeDAO.remove(place);
				return null;
			}
		});

	}

}
