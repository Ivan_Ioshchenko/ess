package ua.kture.service.mail;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message.RecipientType;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;

public class MailSend {

	private static final Logger logger = Logger.getLogger(MailSend.class);

	public void sendMail(String[] recipient, String subject, String text) {
		InternetAddress[] addressRecipient = new InternetAddress[recipient.length];
		try {
			convertRecipientToInetAddr(recipient, addressRecipient);
		} catch (AddressException e) {
			logger.debug("Error convert String recipient to internet address",
					e);
		}

		// Get the session object
		Properties prop = new Properties();
		prop.put("mail.smtp.host", Config.HOST);
		prop.put("mail.smtp.socketFactory.port", Config.PORT);
		prop.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		prop.put("mail.smtp.auth", true);
		prop.put("mail.smtp.port", Config.PORT);
		Session session = Session.getDefaultInstance(prop, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(Config.FROM, Config.PASSWORD);
			}
		});

		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(Config.FROM));
			message.setRecipients(RecipientType.TO, addressRecipient);
			message.setSubject(subject);
			message.setText(text);

			// Send message
			Transport.send(message);
			logger.info("Mail sent successfully.");
		} catch (Exception e) {
			logger.info("Send mail failed.", e);
		}
	}

	private static void convertRecipientToInetAddr(String[] recipient,
			InternetAddress[] addressTo) throws AddressException {
		for (int i = 0; i < recipient.length; i++) {
			addressTo[i] = new InternetAddress(recipient[i]);
		}
	}
}
