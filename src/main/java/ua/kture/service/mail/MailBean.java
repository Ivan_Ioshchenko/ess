package ua.kture.service.mail;

import org.apache.log4j.Logger;

public class MailBean {

	private static final Logger logger = Logger.getLogger(MailBean.class);

	// Config information
	private String from;
	private String host;
	private int port;

	// User infromation
	private String password;
	private String recipients[];
	private String subject;
	private String text;

	public MailBean() {
		from = Config.FROM;
		password = Config.PASSWORD;
		host = Config.HOST;
		port = Integer.valueOf(Config.PORT);
		logger.info("From: " + from + ", password" + password + ", host: "
				+ host + ", port: " + port);
	}

	public String[] getRecipients() {
		return recipients;
	}

	public void setRecipients(String[] recipients) {
		this.recipients = recipients;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
