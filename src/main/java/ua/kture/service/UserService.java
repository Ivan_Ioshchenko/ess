package ua.kture.service;

import ua.kture.db.JDBCTransactionManager;
import ua.kture.db.TransactionManager;
import ua.kture.db.TransactionOperation;
import ua.kture.db.DAO.UserDAO;
import ua.kture.db.DAO.MySQL.UserDAOImpl;
import ua.kture.entity.User;

public class UserService {
	private UserDAO userDAO = new UserDAOImpl();;
	private TransactionManager transactionManager = new JDBCTransactionManager();

	public boolean exist(String email) {
		User value = get(email);
		return value == null ? false : true;
	}

	public void add(final User user) {
		transactionManager.doTransaction(new TransactionOperation() {

			@Override
			public Object execute() {
				userDAO.add(user);
				return null;
			}
		});
	}

	public User get(final String email) {
		User user = (User) transactionManager
				.doTransaction(new TransactionOperation() {

					@Override
					public Object execute() {
						return userDAO.get(email);
					}
				});

		return user;
	}

	public void recoveryPassword(String email, String password) {
		userDAO.update(email, password);
	}

}
