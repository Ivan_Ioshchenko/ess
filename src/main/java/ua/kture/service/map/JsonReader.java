package ua.kture.service.map;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Class for parse request url to json object.
 * 
 * @author Ivan Ioshchenko.
 * 
 */
public class JsonReader {
	/**
	 * Parse request url to JSONObject.
	 * 
	 * @param url
	 *            - url request
	 * @return JSONObject
	 * @throws IOException
	 * @throws JSONException
	 */
	public static JSONObject read(final String url) throws IOException,
			JSONException {
		final InputStream inputStream = new URL(url).openStream();
		try {
			final BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader(inputStream, Charset.forName("UTF-8")));
			final String jsonText = readAll(bufferedReader);
			final JSONObject json = new JSONObject(jsonText);
			return json;
		} finally {
			inputStream.close();
		}
	}

	private static String readAll(final Reader reader) throws IOException {
		final StringBuilder stringBuilder = new StringBuilder();
		int simbol;
		while ((simbol = reader.read()) != -1) {
			stringBuilder.append((char) simbol);
		}
		return stringBuilder.toString();
	}

}
