package ua.kture.img;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Properties;
import javax.servlet.http.Part;

public class PlaceImageStorage implements ImageStorage {

	private String imageDirectory;
	private String defaultImagePath;

	public PlaceImageStorage() throws IOException {
		init();
	}

	@Override
	public void saveFile(Part filePart, String name) throws IOException {
		String path = imageDirectory + name + ".jpg";
		File image = new File(path);
		OutputStream out = new FileOutputStream(image);
		InputStream in = filePart.getInputStream();
		
		byte[] buffer = new byte[1024];
		int len;
		while ((len = in.read(buffer)) != -1) {
		    out.write(buffer, 0, len);
		}
		
		out.close();
	}

	@Override
	public File getFileOrDefault(String name) throws IOException {
		File file = new File(imageDirectory + name);
		return file.exists() ? file : getDefaultAvatarImage();
	}

	private File getDefaultAvatarImage() {
		return new File(defaultImagePath);
	}

	private void init() throws IOException {
		Properties property = new Properties();
		InputStream stream = this.getClass().getResourceAsStream(
				"/config.properties");
		property.load(stream);
		imageDirectory = property.getProperty("image.directory");
		defaultImagePath = property.getProperty("image.default");
		stream.close();
	}
}
