package ua.kture.img;


import java.io.File;
import java.io.IOException;

import javax.servlet.http.Part;

public interface ImageStorage {

	public void saveFile(Part filePart, String login) throws IOException;

	public File getFileOrDefault(String name) throws IOException;

}
