package ua.kture.db;

import java.sql.Connection;
import java.sql.SQLException;
import javax.naming.NamingException;
import org.apache.log4j.Logger;

import ua.kture.db.DAO.DAOExeption;

public class JDBCTransactionManager implements TransactionManager {
	private Logger logger = Logger.getLogger(JDBCTransactionManager.class);

	@Override
	public Object doTransaction(TransactionOperation operation) {
		Object result = null;
		Connection connection = null;
		try {
			connection = JDBCManager.getConnection();
			JDBCConnectionHolder.setConnection(connection);
			result = operation.execute();
			connection.commit();
		} catch (SQLException | NamingException | DAOExeption e) {
			logger.error("Operation with DB failed.", e);

			if (connection != null) {
				try {
					connection.rollback();
				} catch (SQLException ex) {
					logger.error("Error rollback connection.", ex);
					throw new DAOExeption();
				}
			}
		} finally {
			JDBCConnectionHolder.removeConnection();
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					logger.error("Error close connection.", e);
					throw new DAOExeption();
				}
			}
		}

		return result;
	}

}
