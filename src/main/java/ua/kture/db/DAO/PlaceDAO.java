package ua.kture.db.DAO;

import java.util.List;

import ua.kture.entity.Place;

public interface PlaceDAO {

	public void addPlace(Place place);

	public Place getPlace(int idPlace);

	public List<Place> getListPlace();

	public List<Place> getListPlace(String searchText);

	public List<Place> getListPlace(String searchText, String sport);

    public List<Place> getListPlaceToConfirm();

	public void update(Place place);

	public void remove(Place place);
		

}
