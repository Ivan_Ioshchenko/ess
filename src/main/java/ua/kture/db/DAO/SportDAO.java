package ua.kture.db.DAO;

import java.util.List;

import ua.kture.entity.Sport;

public interface SportDAO {
	public void add(Sport sport);

	public List<Sport> getListSport();
	
	public Sport get(int id);
	

}
