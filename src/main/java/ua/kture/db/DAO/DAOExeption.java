package ua.kture.db.DAO;

public class DAOExeption extends RuntimeException {
	public DAOExeption() {
		super();
	}

	public DAOExeption(String message) {
		super(message);
	}

	public DAOExeption(String message, Throwable reason) {
		super(message, reason);
	}

}
