package ua.kture.db.DAO.MySQL;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

public class ConnectionHelper {
	private static Logger logger = Logger.getLogger(ConnectionHelper.class);

	public static void closeStatement(PreparedStatement statement,
			String message) {
		try {
			if (statement != null) {
				statement.close();
			}
		} catch (SQLException e) {
			logger.error(message, e);
		}
	}

	public static void closeStatement(Statement statement, String message) {
		try {
			if (statement != null) {
				statement.close();
			}
		} catch (SQLException e) {
			logger.error(message, e);
		}
	}

}
