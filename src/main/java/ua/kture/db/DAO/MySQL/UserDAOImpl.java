package ua.kture.db.DAO.MySQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import ua.kture.db.JDBCConnectionHolder;
import ua.kture.db.DAO.DAOExeption;
import ua.kture.db.DAO.UserDAO;
import ua.kture.entity.User;

public class UserDAOImpl implements UserDAO {
	private final String SELECT_USER_BY_EMAIL = "SELECT * FROM users WHERE email=?;";
	private final String INSERT_USER = "INSERT INTO users VALUES (default, ?, ?, ?, ?);";
	private final String UPDATE_USER = "UPDATE users SET password=?"
			+ "	WHERE email=?";

	public UserDAOImpl() {
	}

	@Override
	public User get(String email) {
		User user = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			Connection connection = JDBCConnectionHolder.getConnection();

			pstmt = connection.prepareStatement(SELECT_USER_BY_EMAIL);
			pstmt.setString(1, email);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				user = unMap(rs);
			}
		} catch (Exception e) {
			throw new DAOExeption("Fail get user.", e);
		} finally {
			ConnectionHelper.closeStatement(pstmt, "Error when get user.");
		}

		return user;
	}

	@Override
	public void add(User user) {
		PreparedStatement pstmt = null;

		try {
			Connection connection = JDBCConnectionHolder.getConnection();

			pstmt = connection.prepareStatement(INSERT_USER);
			pstmt.setString(1, user.getFirstName());
			pstmt.setString(2, user.getLastName());
			pstmt.setString(3, user.getEmail());
			pstmt.setString(4, user.getPassword());

			pstmt.executeUpdate();
		} catch (Exception e) {
			throw new DAOExeption("Fail add user.", e);
		} finally {
			ConnectionHelper.closeStatement(pstmt, "Error when add user.");
		}

	}

	@Override
	public void update(String email, String password) {

		PreparedStatement pstmt = null;

		try {
			Connection connection = JDBCConnectionHolder.getConnection();
			pstmt = connection.prepareStatement(UPDATE_USER);
			pstmt.setString(1, password);
			pstmt.setString(2, email);

			pstmt.executeUpdate();
			pstmt.close();
		} catch (Exception e) {
			throw new DAOExeption("Fail update user.", e);
		} finally {
			ConnectionHelper.closeStatement(pstmt, "Error when update user.");
		}

	}

	private User unMap(ResultSet rs) throws SQLException {
		User user = new User();
		user.setFirstName(rs.getString("first_name"));
		user.setLastName(rs.getString("last_name"));
		user.setPassword(rs.getString("password"));
		user.setEmail(rs.getString("email"));

		return user;
	}

}
