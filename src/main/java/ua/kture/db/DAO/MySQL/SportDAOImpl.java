package ua.kture.db.DAO.MySQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import ua.kture.db.JDBCConnectionHolder;
import ua.kture.db.DAO.DAOExeption;
import ua.kture.db.DAO.SportDAO;

import ua.kture.entity.Sport;

public class SportDAOImpl implements SportDAO {

	private final String SELECT_SPORTS = "SELECT * FROM sports;";
	private final String SELECT_SPORT = "SELECT * FROM sports WHERE id=?;";
	private final String INSERT_SPORT = "INSERT INTO users VALUES (default, ?);";

	@Override
	public void add(Sport sport) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Sport> getListSport() {
		List<Sport> list = new ArrayList<>();
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			Connection connection = JDBCConnectionHolder.getConnection();
			pstmt = connection.prepareStatement(SELECT_SPORTS);

			rs = pstmt.executeQuery();
			while (rs.next()) {
				list.add(extractSport(rs));
			}
		} catch (Exception e) {
			throw new DAOExeption("Fail get list sport.", e);
		} finally {
			ConnectionHelper
					.closeStatement(pstmt, "Error when get list sport.");
		}

		return list;

	}

	private Sport extractSport(ResultSet rs) throws SQLException {
		Sport sport = new Sport(rs.getInt("id"), rs.getString("name"));
		return sport;
	}

	@Override
	public Sport get(int id) {
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		Sport sport=null;
		try {
			Connection connection = JDBCConnectionHolder.getConnection();
			pstmt = connection.prepareStatement(SELECT_SPORT);
			pstmt.setInt(1, id);
			
			rs = pstmt.executeQuery();
			if (rs.next()) {
				sport = extractSport(rs);
			}
		} catch (Exception e) {
			throw new DAOExeption("Fail get  sport.", e);
		} finally {
			ConnectionHelper
					.closeStatement(pstmt, "Error when get sport.");
		}
		return sport;
	}

}
