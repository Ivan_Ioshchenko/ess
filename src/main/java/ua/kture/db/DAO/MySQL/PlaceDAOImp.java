package ua.kture.db.DAO.MySQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ua.kture.db.JDBCConnectionHolder;
import ua.kture.db.DAO.DAOExeption;
import ua.kture.db.DAO.PlaceDAO;
import ua.kture.entity.Place;

public class PlaceDAOImp implements PlaceDAO {

	private final String SELECT_CONFIRM_PLACES = "SELECT * FROM places WHERE visible IS NOT NULL;";
	private final String SELECT_NOT_CONFIRM_PLACES = "SELECT * FROM places WHERE visible IS NULL;";
	private final String INSERT_PLACE = "INSERT INTO places VALUES (default, ?, ?, ?, ?, ?, ?, ?,?);";
	private final String SELECT_PLACE_BY_ID = "SELECT * FROM places WHERE id=?";
	private final String DLETE_PLACE = "DELETE FROM places WHERE id=?";
	private final String UPDATE_PLACE = "UPDATE places SET telephone=?, price=?, description=?, address=?,  position=?, "
			+ "place_type=?,  sport_id=?,  visible=? WHERE id=?";

	@Override
	public List<Place> getListPlaceToConfirm() {
		List<Place> list = new ArrayList<>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			Connection connection = JDBCConnectionHolder.getConnection();

			pstmt = connection.prepareStatement(SELECT_NOT_CONFIRM_PLACES);

			rs = pstmt.executeQuery();
			while (rs.next()) {
				list.add(unMap(rs));
			}
		} catch (Exception e) {
			throw new DAOExeption("Fail to get list place.", e);
		} finally {
			ConnectionHelper
					.closeStatement(pstmt, "Error when get list place.");
		}

		return list;
	}

	@Override
	public void addPlace(Place place) {
		PreparedStatement pstmt = null;
		try {
			Connection connection = JDBCConnectionHolder.getConnection();
			pstmt = connection.prepareStatement(INSERT_PLACE);
			map(place, pstmt);

			pstmt.executeUpdate();
		} catch (Exception e) {
			throw new DAOExeption("Fail to add place.", e);
		} finally {
			ConnectionHelper.closeStatement(pstmt, "Error when add place.");
		}

	}

	@Override
	public Place getPlace(int idPlace) {
		Place place = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			Connection connection = JDBCConnectionHolder.getConnection();

			pstmt = connection.prepareStatement(SELECT_PLACE_BY_ID);
			pstmt.setInt(1, idPlace);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				place = unMap(rs);
			}

		} catch (Exception e) {
			throw new DAOExeption("Fail to get place.", e);
		} finally {
			ConnectionHelper.closeStatement(pstmt, "Error when get place.");
		}

		return place;
	}

	@Override
	public List<Place> getListPlace() {
		List<Place> list = new ArrayList<>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			Connection connection = JDBCConnectionHolder.getConnection();

			pstmt = connection.prepareStatement(SELECT_CONFIRM_PLACES);

			rs = pstmt.executeQuery();
			while (rs.next()) {
				list.add(unMap(rs));
			}
		} catch (Exception e) {
			throw new DAOExeption("Fail to get list place.", e);
		} finally {
			ConnectionHelper
					.closeStatement(pstmt, "Error when get list place.");
		}

		return list;
	}

	private Place unMap(ResultSet rs) throws SQLException {
		Place place = new Place();
		place.setId(rs.getInt("places.id"));
		place.setTelephone(rs.getString("places.telephone"));
		place.setDescription(rs.getString("places.description"));
		place.setPosition(rs.getString("places.position"));
		place.setAddress(rs.getString("places.address"));
		place.setPrice(rs.getDouble("places.price"));
		place.setSport(new SportDAOImpl().get(rs.getInt("places.sport_id")));
		place.setPlaceType(rs.getString("places.place_type"));
		place.setVisible(rs.getString("places.visible"));
		return place;
	}

	private void map(Place place, PreparedStatement pstmt) throws SQLException {
		pstmt.setString(1, place.getTelephone());
		pstmt.setDouble(2, place.getPrice());
		pstmt.setString(3, place.getDescription());
		pstmt.setString(4, place.getAddress());
		pstmt.setString(5, place.getPosition());
		pstmt.setString(6, place.getPlaceType());
		pstmt.setInt(7, place.getSport().getId());
		pstmt.setString(8, place.getVisible());

	}

	@Override
	public List<Place> getListPlace(String searchText) {
		StringBuilder query = new StringBuilder(
				"SELECT * FROM ess.places, ess.sports  WHERE places.sport_id = sports.id AND");
		query.append(" telephone LIKE '%").append(searchText).append("%' ");
		query.append(" OR description LIKE '%").append(searchText)
				.append("%' ");
		query.append(" OR address LIKE '%").append(searchText).append("%' ");
		query.append(" OR name LIKE '%").append(searchText).append("%' ");

		query.append("GROUP BY places.id;");

		List<Place> list = new ArrayList<>();
		Statement st = null;
		ResultSet rs = null;

		try {
			Connection connection = JDBCConnectionHolder.getConnection();
			st = connection.createStatement();
			rs = st.executeQuery(query.toString());

			while (rs.next()) {
				list.add(unMap(rs));
			}
		} catch (Exception e) {
			throw new DAOExeption("Fail to get list place.", e);
		} finally {
			ConnectionHelper.closeStatement(st, "Error when get list place.");
		}

		return list;

	}

	@Override
	public List<Place> getListPlace(String searchText, String sport) {
		StringBuilder query = new StringBuilder(
				"SELECT * FROM ess.places, ess.sports  WHERE places.sport_id = sports.id AND ");
		query.append(" sports.id = ").append(sport);

		if (searchText != null && searchText.trim().length() > 0) {
			query.append(" AND telephone LIKE '%").append(searchText)
					.append("%' ");
			query.append(" OR description LIKE '%").append(searchText)
					.append("%' ");
			query.append(" OR address LIKE '%").append(searchText)
					.append("%' ");
		}

		query.append(" GROUP BY places.id;");

		List<Place> list = new ArrayList<>();
		Statement st = null;
		ResultSet rs = null;

		try {
			Connection connection = JDBCConnectionHolder.getConnection();
			st = connection.createStatement();
			rs = st.executeQuery(query.toString());

			while (rs.next()) {
				list.add(unMap(rs));
			}
		} catch (Exception e) {
			throw new DAOExeption("Fail to get list place.", e);
		} finally {
			ConnectionHelper.closeStatement(st, "Error when get list place.");
		}

		return list;
	}

	@Override
	public void update(Place place) {
		PreparedStatement pstmt = null;
		try {
			Connection connection = JDBCConnectionHolder.getConnection();
			pstmt = connection.prepareStatement(UPDATE_PLACE);
			map(place, pstmt);
			pstmt.setInt(9, place.getId());
			pstmt.executeUpdate();
		} catch (Exception e) {
			throw new DAOExeption("Fail to update place.", e);
		} finally {
			ConnectionHelper.closeStatement(pstmt, "Error when update place.");
		}

	}

	@Override
	public void remove(Place place) {
		PreparedStatement pstmt = null;

		try {
			Connection connection = JDBCConnectionHolder.getConnection();

			pstmt = connection.prepareStatement(DLETE_PLACE);
			pstmt.setInt(1, place.getId());
			pstmt.executeUpdate();

		} catch (Exception e) {
			throw new DAOExeption("Fail to delete place.", e);
		} finally {
			ConnectionHelper.closeStatement(pstmt, "Error when delete place.");
		}
	}

}
