package ua.kture.db;

public interface TransactionOperation {
	public Object execute();

}
