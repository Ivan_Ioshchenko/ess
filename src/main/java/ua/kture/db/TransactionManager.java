package ua.kture.db;


public interface TransactionManager {
	public Object doTransaction(TransactionOperation operation);

}
